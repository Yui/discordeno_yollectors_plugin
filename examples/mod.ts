import {
  ApplicationCommandOptionTypes,
  ButtonStyles,
  createBot,
  GatewayIntents,
  InteractionResponseTypes,
  MessageComponentTypes,
  startBot,
  TextStyles,
} from "https://deno.land/x/discordeno@17.0.0/mod.ts";
import { load } from "https://x.nest.land/Yenv@1.0.0/mod.ts";
import { enableCollectorsPlugin } from "../src/mod.ts";

const env = await load({
  token: /[M-Z][A-Za-z\d]{23}\.[\w-]{6}\.[\w-]{27}/,
  botid: {
    type: BigInt,
    optional: true,
  },
});

const baseBot = createBot({
  token: env.token,
  intents: GatewayIntents.Guilds | GatewayIntents.GuildMessageReactions,
  botId: env.botid ?? BigInt(atob(env.token.split(".")[0])),
  events: {
    async interactionCreate(_, interaction) {
      if (interaction.data?.name === "test") {
        const opt = interaction.data.options?.[0];
        let type: "modal" | "component" | "reaction" = "modal";

        if (opt && opt?.type == ApplicationCommandOptionTypes.String) {
          type = opt.value as "modal" | "component" | "reaction";
        }

        if (type === "component") {
          // Send button or some other component
          await bot.helpers.sendInteractionResponse(interaction.id, interaction.token, {
            type: InteractionResponseTypes.ChannelMessageWithSource,
            data: {
              content: "test",
              components: [
                {
                  type: MessageComponentTypes.ActionRow,
                  components: [
                    {
                      type: MessageComponentTypes.Button,
                      label: "test",
                      customId: "mybuttonid",
                      style: ButtonStyles.Primary,
                      disabled: false,
                    },
                  ],
                },
              ],
            },
          });

          const collected = await bot.collectors
            .collect("component", {
              id: interaction.id,
            })
            .catch(console.error);

          if (!collected) return console.log("Collector expired");

          await bot.helpers.sendFollowupMessage(collected.interaction.token, {
            type: InteractionResponseTypes.ChannelMessageWithSource,
            data: {
              content: "You pressed the button.",
            },
          });
        } else if (type === "modal") {
          // Send modal
          await bot.helpers.sendInteractionResponse(interaction.id, interaction.token, {
            type: InteractionResponseTypes.Modal,
            data: {
              title: "modal testing",
              customId: "modaltest",
              components: [
                {
                  type: MessageComponentTypes.ActionRow,
                  components: [
                    {
                      type: MessageComponentTypes.InputText,
                      style: TextStyles.Short,
                      customId: "whateveryouwant",
                      label: "What is your name?",
                    },
                  ],
                },
              ],
            },
          });

          const collectedModal = await bot.collectors
            .collect("modal", {
              id: "modaltest",
            })
            .catch(console.error);

          if (!collectedModal) return console.log("Collector expired");

          if (
            collectedModal.interaction.data?.components?.[0]?.components?.[0].type == MessageComponentTypes.InputText
          ) {
            await bot.helpers.sendFollowupMessage(collectedModal.interaction.token, {
              type: InteractionResponseTypes.ChannelMessageWithSource,
              data: {
                content: `Received name ${collectedModal.interaction.data.components[0].components[0].value}`,
              },
            });
          }
        } else {
          // Check if we are in a channel
          if (!interaction.channelId)
            return await bot.helpers.sendInteractionResponse(interaction.id, interaction.token, {
              type: InteractionResponseTypes.ChannelMessageWithSource,
              data: {
                content: "Cannot send message to this channel",
              },
            });

          // Send a test message
          const { id } = await bot.helpers.sendMessage(interaction.channelId, {
            content: "React to this message.",
          });

          // Add a reaction to the message
          await bot.helpers.addReaction(interaction.channelId, id, "🎉");

          // Create a collecor
          bot.collectors
            .collect("reaction", {
              id,
              collectCondition: (data) => data.userId == interaction.user.id,
            })
            .catch(console.error)
            .then(async (val) => {
              // Handle collector expiraction
              if (!val) {
                console.log("Collector expired");
                await bot.helpers.editMessage(interaction.channelId!, id, {
                  content: "Collector expired.",
                });
                await bot.helpers.deleteReactionsAll(interaction.channelId!, id);
                return;
              }

              // Do a thing if collector finishes
              await bot.helpers.sendMessage(interaction.channelId!, {
                content: `Collected reaction ${val.emoji.name ?? val.emoji.id} from user ${
                  val.user?.username ? val.user.username : `\`${val.userId}\``
                }`,
                messageReference: {
                  messageId: id,
                  failIfNotExists: false,
                },
              });
            });

          // Tell user to react to it.
          await bot.helpers.sendInteractionResponse(interaction.id, interaction.token, {
            type: InteractionResponseTypes.ChannelMessageWithSource,
            data: {
              content: "Message created. React to it.",
              flags: 64,
            },
          });
        }
      }
    },
  },
});

const bot = enableCollectorsPlugin(baseBot, {
  collectionExpirationCheckInterval: 30000,
  defaultCollectorExpiration: 30000,
});

console.log("Starting bot");
await startBot(bot);

console.log("Updating commands");
if (!(await bot.helpers.getGlobalApplicationCommands()).find((c) => c.name === "test"))
  await bot.helpers.upsertGlobalApplicationCommands([
    {
      name: "test",
      description: "test",
      type: 1,
      options: [
        {
          name: "option",
          description: "test with modal or component",
          type: ApplicationCommandOptionTypes.String,
          choices: [
            {
              name: "modal",
              value: "modal",
            },
            {
              name: "component",
              value: "component",
            },
            {
              name: "reaction",
              value: "reaction",
            },
          ],
          required: false,
        },
      ],
    },
  ]);
