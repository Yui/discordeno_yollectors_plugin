// deno-lint-ignore ban-types
export function assign<T extends {}, U>(target: T, source: U): asserts target is T & U {
  Object.assign(target, source);
}
