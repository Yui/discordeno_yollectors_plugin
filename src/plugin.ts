import { componentCollectorOptions, componentCollectors, createCompCollector } from "./collectors/component.ts";
import {
  createMessageReactionCollector,
  messageReactionCollectorOptions,
  messageReactionCollectors,
  messageReactionData,
} from "./collectors/messageReaction.ts";
import { createModalCollector, modalCollectorOptions, modalCollectors } from "./collectors/modal.ts";
import { Bot, Interaction, InteractionResponseTypes, InteractionTypes } from "./deps.ts";
import type { collectorsPlugin } from "./types/mod.ts";
import { assign } from "./utils.ts";

const collectors: collectorsPlugin["collectors"] = {
  // @ts-ignore 2719
  collect: (type, options) => {
    if (type === "component") return createCompCollector(options as componentCollectorOptions);
    else if (type === "modal") return createModalCollector(options as modalCollectorOptions);
    else return createMessageReactionCollector(options as messageReactionCollectorOptions);
  },
  get componentCollectors() {
    return componentCollectors;
  },
  get modalCollectors() {
    return modalCollectors;
  },
  get messageReactionCollectors() {
    return messageReactionCollectors;
  },
};

export let defaultCollectorExpiration = 60000;

async function handleInteractionCreateEvent(bot: Bot & collectorsPlugin, interaction: Interaction) {
  if (interaction.type == InteractionTypes.MessageComponent) {
    // 1. nested interaction
    // 2. normal interaction
    // 3. message (Not interaction)

    let collector;

    if (interaction.message?.messageReference?.messageId)
      collector = bot.collectors.componentCollectors.get(interaction.message.messageReference.messageId);

    if (!collector && interaction.message?.interaction?.id)
      collector = bot.collectors.componentCollectors.get(interaction.message.interaction.id);

    if (!collector && interaction.message?.id)
      collector = bot.collectors.componentCollectors.get(interaction.message.id);

    if (!collector) return;

    const collectorReturn = {
      customId: interaction.data?.customId!,
      interaction: interaction,
    };

    if (collector.collectCondition) {
      const passed = collector.collectCondition(collectorReturn);
      if (!passed) return;
    }

    collector.state?.collected?.push(interaction);

    // Acknowledge
    let shouldDefer = false;
    if (typeof collector.state.defer === "function") {
      shouldDefer = collector.state.defer(collectorReturn);
    } else {
      shouldDefer = collector.state.defer ?? true;
    }

    if (shouldDefer) {
      await bot.helpers
        .sendInteractionResponse(interaction.id, interaction.token, {
          type: InteractionResponseTypes.DeferredUpdateMessage,
        })
        .catch(console.error);
    }

    if (collector.resolveCondition) {
      const passed = collector.resolveCondition(collectorReturn);
      if (!passed) return;
    }

    collector.resolve(collectorReturn);
    componentCollectors.delete(collector.key);
  } else if (interaction.type == InteractionTypes.ModalSubmit) {
    const collector = interaction.data?.customId && modalCollectors.get(interaction.data.customId);
    if (!collector) return;

    const collectorReturn = {
      interaction: interaction,
    };

    if (collector.collectCondition) {
      const passed = collector.collectCondition(collectorReturn);
      if (!passed) return;
    }

    // Acknowledge
    await bot.helpers
      .sendInteractionResponse(interaction.id, interaction.token, {
        type: InteractionResponseTypes.DeferredUpdateMessage,
      })
      .catch(console.error);

    // Modal does not have resolveCondition

    collector.resolve(collectorReturn);
    modalCollectors.delete(collector.key);
  }
}

function handleMessageReactionAddEvent(data: messageReactionData) {
  const collector = messageReactionCollectors.get(data.messageId);
  if (!collector) return;

  if (collector.collectCondition) {
    const passed = collector.collectCondition(data);
    if (!passed) return;
  }

  collector.state.collected.push(data);

  if (collector.resolveCondition) {
    const passed = collector.resolveCondition(data);
    if (!passed) return;
  }

  collector.resolve(data);
  messageReactionCollectors.delete(collector.key);
}

function handleCollectorExpiration() {
  const now = Date.now();

  [componentCollectors, modalCollectors, messageReactionCollectors].forEach((collectorsStorage) => {
    collectorsStorage.forEach((collector) => {
      if (!collector.expires || now < collector.expires) return;
      // @ts-ignore too lazy to bother with this
      collectorsStorage.delete(collector.key);
      collector.reject("Collector not resolved in time.");
    });
  });
}

export function enableCollectorsPlugin<T extends Bot>(
  bot: T,
  options?: {
    /** If not than specified then default is infinite */
    defaultCollectorExpiration?: number;
    /** If not specified then this check is disabled */
    collectionExpirationCheckInterval?: number;
    /** Disables the hooks for some events.
     *
     * `true` - disables all event hooks
     *
     * `false` | `undefined` - all stay enabled
     *
     * `["specific hook"]` - specified hooks get disabled
     *
     * If you disable `interactionCreate` hook than you need to call `bot.collectors.handleInteractionCreateEvent` function manually in your interactionCreate event.
     *
     * If you disable `mesageReactionAdd` hook than you need to call `bot.collectors.handleMessageReactionAddEvent` function manually in your messageReactionAdd event.
     * */
    disableEventHooks?: boolean | ("interactionCreate" | "messageReactionAdd")[];
  }
): T & collectorsPlugin {
  // Set stuff
  if (options?.defaultCollectorExpiration) defaultCollectorExpiration = options.defaultCollectorExpiration;

  assign(bot, {
    collectors,
  });

  bot.collectors.handleInteractionCreateEvent = (...params) => {
    return handleInteractionCreateEvent(bot, ...params);
  };

  bot.collectors.handleMessageReactionAddEvent = (...params) => {
    return handleMessageReactionAddEvent(...params);
  };

  // Setup events
  const old = {
    interactionCreate: bot.events.interactionCreate,
    reactionAdd: bot.events.reactionAdd,
  };

  const hooksToDo = new Set<"interactionCreate" | "messageReactionAdd">(["interactionCreate", "messageReactionAdd"]);

  if (options?.disableEventHooks == true) hooksToDo.clear();
  else if (Array.isArray(options?.disableEventHooks)) options?.disableEventHooks.forEach((c) => hooksToDo.delete(c));

  if (hooksToDo.has("interactionCreate")) {
    bot.events.interactionCreate = (_, interaction) => {
      if ([InteractionTypes.MessageComponent, InteractionTypes.ModalSubmit].includes(interaction.type))
        bot.collectors.handleInteractionCreateEvent(interaction);
      return old.interactionCreate(bot, interaction);
    };
  }

  if (hooksToDo.has("messageReactionAdd")) {
    bot.events.reactionAdd = (_, data) => {
      bot.collectors.handleMessageReactionAddEvent(data);
      return old.reactionAdd(bot, data);
    };
  }

  // Check expiration
  if (options?.collectionExpirationCheckInterval)
    setInterval(handleCollectorExpiration, options?.collectionExpirationCheckInterval);

  return bot;
}
