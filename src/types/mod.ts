import { Interaction } from "../deps.ts";

import { componentCollectorOptions, componentCollectorReturn } from "../collectors/component.ts";
import {
  messageReactionCollectorOptions,
  messageReactionCollectorReturn,
  messageReactionData,
} from "../collectors/messageReaction.ts";
import { modalCollectorOptions, modalCollectorReturn } from "../collectors/modal.ts";

// deno-lint-ignore no-explicit-any
export interface baseCollector<T, E, Z extends Record<string, any> | undefined = undefined> {
  key: E;
  expires?: number;
  state: Z;
  collectCondition?: (value: T) => boolean;
  resolveCondition?: (value: T) => boolean;
  resolve: (value: T) => void;
  // deno-lint-ignore no-explicit-any
  reject: (reason?: any) => void;
}
// deno-lint-ignore no-explicit-any
export type collectorStorage<T, E, Z extends Record<string, any> | undefined = undefined> = Map<
  E,
  baseCollector<T, E, Z>
>;

export type componentCollector = baseCollector<
  {
    customId: string;
    interaction: Interaction;
  },
  bigint,
  {
    collected: Interaction[];
    /**
     * Defer when collecting.
     * Default: true
     */
    defer?: boolean | ((value: { customId: string; interaction: Interaction }) => boolean);
  }
>;

export type modalCollector = baseCollector<
  {
    interaction: Interaction;
  },
  string
>;

export type messageReactionCollector = baseCollector<
  messageReactionData,
  bigint,
  {
    collected: messageReactionData[];
  }
>;

export interface collectorsPlugin {
  collectors: {
    collect<T extends "component" | "modal" | "reaction">(
      type: T,
      options: T extends "component"
        ? componentCollectorOptions
        : T extends "modal"
        ? modalCollectorOptions
        : messageReactionCollectorOptions
    ): T extends "component"
      ? componentCollectorReturn
      : T extends "modal"
      ? modalCollectorReturn
      : messageReactionCollectorReturn;
    componentCollectors: Map<bigint, componentCollector>;
    modalCollectors: Map<string, modalCollector>;
    messageReactionCollectors: Map<bigint, messageReactionCollector>;
    handleInteractionCreateEvent: (interaction: Interaction) => Promise<void>;
    handleMessageReactionAddEvent: (data: messageReactionData) => void;
  };
}

export type AtLeastOne<T, U = { [K in keyof T]: Pick<T, K> }> = Partial<T> & U[keyof U];
