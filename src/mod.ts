export type { componentCollectorOptions, componentCollectorReturn } from "./collectors/component.ts";
export type { modalCollectorOptions, modalCollectorReturn } from "./collectors/modal.ts";
export * from "./plugin.ts";
