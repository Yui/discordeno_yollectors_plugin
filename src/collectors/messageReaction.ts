import { EventHandlers } from "../deps.ts";
import { messageReactionCollector } from "../types/mod.ts";
import { createBaseCollector } from "./base.ts";

export type messageReactionData = Parameters<EventHandlers["reactionAdd"]>[1];

export const messageReactionCollectors = new Map<bigint, messageReactionCollector>();

export type messageReactionCollectorOptions = Partial<{
  expires: number;
  collectCondition: messageReactionCollector["collectCondition"];
  resolveCondition: messageReactionCollector["resolveCondition"];
}> & {
  /** id of the message */
  id: bigint;
};

export type messageReactionCollectorReturn = ReturnType<typeof createMessageReactionCollector>;

export function createMessageReactionCollector(options: messageReactionCollectorOptions) {
  return createBaseCollector(options.id, messageReactionCollectors, {
    ttl: options?.expires,
    collectCondition: options?.collectCondition,
    resolveCondition: options?.resolveCondition,
    collectorState: {
      collected: [],
    },
  });
}
