import type { modalCollector } from "../types/mod.ts";
import { createBaseCollector } from "./base.ts";

export const modalCollectors = new Map<string, modalCollector>();

export type modalCollectorOptions = Partial<{
  expires: number;
  collectCondition: modalCollector["collectCondition"];
}> & {
  /** custom id of the modal */
  id: string;
};

export type modalCollectorReturn = ReturnType<typeof createModalCollector>;

export function createModalCollector(options: modalCollectorOptions) {
  return createBaseCollector(options.id, modalCollectors, {
    ttl: options?.expires,
    collectCondition: options?.collectCondition,
    collectorState: undefined,
  });
}
