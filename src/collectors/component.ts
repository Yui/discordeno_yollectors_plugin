import { componentCollector } from "../types/mod.ts";
import { createBaseCollector } from "./base.ts";

export const componentCollectors = new Map<bigint, componentCollector>();

export type componentCollectorOptions = Partial<{
  expires: number;
  collectCondition: componentCollector["collectCondition"];
  resolveCondition: componentCollector["resolveCondition"];
  /**
   * Defer when collecting.
   * Default: true
   */
  defer: componentCollector["state"]["defer"];
}> & {
  /** id of the interaction */
  id: bigint;
};

export type componentCollectorReturn = ReturnType<typeof createCompCollector>;

export function createCompCollector(options: componentCollectorOptions) {
  return createBaseCollector(options.id, componentCollectors, {
    ttl: options?.expires,
    collectCondition: options?.collectCondition,
    resolveCondition: options?.resolveCondition,
    collectorState: {
      collected: [],
      defer: options.defer,
    },
  });
}
