import { defaultCollectorExpiration } from "../plugin.ts";
import { AtLeastOne, baseCollector, collectorStorage } from "../types/mod.ts";

// deno-lint-ignore no-explicit-any
export function createBaseCollector<T, E extends any = never, Z extends Record<string, any> | undefined = undefined>(
  key: E,
  storage: collectorStorage<T, E, Z>,
  options?: AtLeastOne<{
    /** When the collector expires in ms. */
    ttl: number;
    collectCondition: baseCollector<T, E, Z>["collectCondition"];
    resolveCondition: baseCollector<T, E, Z>["resolveCondition"];
    collectorState: Z;
  }>
): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    storage.set(key, {
      key: key,
      expires: Date.now() + (options?.ttl ?? defaultCollectorExpiration),
      resolve,
      reject,
      collectCondition: options?.collectCondition,
      resolveCondition: options?.resolveCondition,
      // @ts-ignore 2322
      state: options?.collectorState,
    });
  });
}
